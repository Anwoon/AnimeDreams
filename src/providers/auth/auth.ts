import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import {Subject} from "rxjs/Subject";
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  private isLoggedIn = false;
  public usernameLog: string;
  public idLog: any;
  private profil: any;
  private email: any;
  private username: any;
  private password: any;
  private recovery: any;
  public subjectRecoveryPassword: Subject<any>;
  public subjectUpdateEmail: Subject<any>;
  public subjectProfil: Subject <any>;
  public subjectUpdateUsername: Subject<any>;
  public subjectUpdatePassword: Subject<any>;
  private apiRegister = 'http://192.168.1.36/BIGPROJECT/API/public/user/register';
  private apiLogin = 'http://192.168.1.36/BIGPROJECT/API/public/user/connect';
  private apiProfil = 'http://192.168.1.36/BIGPROJECT/API/public/user/profil/';
  private apiRecoveryPassword = 'http://192.168.1.36/BIGPROJECT/API/public/user/recovery';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(public http: HttpClient) {
    this.subjectProfil = new Subject<any>();
    this.subjectUpdateEmail = new Subject<any>();
    this.subjectUpdateUsername = new Subject<any>();
    this.subjectUpdatePassword = new Subject<any>();
    this.subjectRecoveryPassword = new Subject<any>();
  }

  getRegister(form): Observable<{}> {
    return this.http.post(this.apiRegister, JSON.stringify(form), this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  getLogin(form): Observable<{}> {
    return this.http.post(this.apiLogin, JSON.stringify(form), this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  getEmitProfil(){
    this.subjectProfil.next(this.profil);
  }

  getProfil(id) {
    this.http.get<any>(this.apiProfil + id).subscribe(
      (data) => { this.profil = data; this.getEmitProfil(); },
      (error) => console.log(error)
    )
  }

  getEmitUpdateEmail() {
    this.subjectUpdateEmail.next(this.email);
  }

  updateEmail(data) {
    this.http.post(this.apiProfil + this.idLog, JSON.stringify({'email':data}), this.httpOptions).subscribe(
      (data) => { this.email = data; this.getEmitUpdateEmail() },
      (error) => console.log(error)
    )
  }

  getEmitUpdateUsername() {
    this.subjectUpdateUsername.next(this.username);
  }

  updateUsername(data) {
    this.http.post(this.apiProfil + this.idLog, JSON.stringify({'username':data}), this.httpOptions).subscribe(
      (data) => { this.username = data; this.getEmitUpdateUsername(); },
      (error) => console.log(error)
    )
  }

  getEmitUpdatePassword() {
    this.subjectUpdatePassword.next(this.password);
  }

  updatePassword(data) {
    this.http.post(this.apiProfil + this.idLog, JSON.stringify({'password':data}), this.httpOptions).subscribe(
      (data) => { this.password = data; this.getEmitUpdatePassword(); },
      (error) => console.log(error)
    )
  }

  getEmitRecoveryPassword() {
    this.subjectRecoveryPassword.next(this.recovery);
  }

  recoveryPassword(data) {
    this.http.post(this.apiRecoveryPassword, JSON.stringify({'email': data}), this.httpOptions).subscribe(
      (data) => { console.log(data); this.recovery = data; this.getEmitRecoveryPassword(); },
      (error) => console.log(error)
    )
  }


  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  login() : void {
    this.isLoggedIn = true;
  }

  logout() : void {
    this.isLoggedIn = false;
  }

  authenticated() : boolean {
    return this.isLoggedIn;
  }


}
