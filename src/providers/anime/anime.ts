import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";
import {LoadingController} from "ionic-angular";
import {YouranimeProvider} from "../youranime/youranime";

/*
  Generated class for the AnimeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AnimeProvider {

  private AllAnime;
  private OneAnime;
  private SearchList;
  private responseAddOne;
  public subjectAllAnime:  Subject<any>;
  public subjectOneAnime:  Subject<any>;
  public subjectAddOneAnime: Subject<any>;
  public subjectSearch: Subject<any>;

  private apiAllAnimes = 'http://192.168.1.36/BIGPROJECT/API/public/animelist';
  private apiSearch = 'http://192.168.1.36/BIGPROJECT/API/public/animelist/search/';
  private apiOneAnime = 'http://192.168.1.36/BIGPROJECT/API/public/anime/';
  private apiAddAnime = 'http://192.168.1.36/BIGPROJECT/API/public/animelist/add/';

  constructor(public http: HttpClient, public yourAnimeService: YouranimeProvider) {
    this.subjectAllAnime = new Subject<any>();
    this.subjectOneAnime = new Subject<any>();
    this.subjectAddOneAnime = new Subject<any>();
    this.subjectSearch = new Subject<any>();
  }

  getAllAnimes() {
    this.http.get<any>(this.apiAllAnimes).subscribe(
      (data) =>  { this.AllAnime = data; this.getEmitAllAnime(); },
      (error) => console.log(error)
    );
  }

  getEmitAllAnime() {
    this.subjectAllAnime.next(this.AllAnime);
  }

  getOneAnime(id) {
    this.http.get<any>(this.apiOneAnime + id).subscribe(
      (data) => { this.OneAnime = data; this.getEmitOneAnime(); },
      (error) => console.log(error),
    )
  }

  getEmitOneAnime() {
    this.subjectOneAnime.next(this.OneAnime);
  }

  getEmitAddResponse() {
    this.subjectAddOneAnime.next(this.responseAddOne);
  }

  //recuperer l'anime ajouter et le push dans le tableau

  addOneAnime(id, idUser, idCategory) {
    this.http.post<any>(this.apiAddAnime + idUser +'/'+ id + '/' + idCategory, {}).subscribe(
      (data) => { this.responseAddOne = data; this.getEmitAddResponse(); this.yourAnimeService.getYourAnime(); },
      (error) => console.log(error),
    )
  }

  getEmitSearch() {
    this.subjectSearch.next(this.SearchList);
  }

  Search(val) {
    this.http.get<any>(this.apiSearch + val).subscribe(
      (data) => {
        this.SearchList = data;
        this.getEmitSearch();},
      (error) => console.log(error)
    );
  }
}
