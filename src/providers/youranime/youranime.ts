import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AuthProvider} from "../auth/auth";
import {Subject} from "rxjs/Subject";
import {stringify} from "@angular/compiler/src/util";

/*
  Generated class for the YouranimeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class YouranimeProvider {

  public subjectYourAnimeOne: Subject<any>;
  public subjectYourAnime: Subject<any>;
  public subjectUpdateOneAnime: Subject<any>;
  public subjectDeleteOneAnime : Subject<any>;
  public subjectUpdateEpisodeAnime: Subject<any>;
  private response: any;
  private OneAnime: any;
  private idLog: number;
  private YourAnime:any = [];
  private apiYourAnime: string = 'http://192.168.1.36/BIGPROJECT/API/public/animelist/';
  private apiUpdateOneAnime: string = 'http://192.168.1.36/BIGPROJECT/API/public/animelist/update/';
  constructor(public http: HttpClient, public authService: AuthProvider) {
    this.subjectYourAnime = new Subject<any>();
    this.subjectYourAnimeOne = new Subject<any>();
    this.subjectUpdateOneAnime = new Subject<any>();
    this.subjectDeleteOneAnime = new Subject<any>();
    this.subjectUpdateEpisodeAnime = new Subject<any>();
    this.idLog = this.authService.idLog;
  }

  getEmitYourAnime() {
    this.subjectYourAnime.next(this.YourAnime);
  }

  getYourAnime() {
    this.http.get(this.apiYourAnime + this.idLog).subscribe(
      (data) => { this.YourAnime = data; this.getEmitYourAnime() },
      (error) => console.log(error)
    )
  }

  getEmitYourAnimeOne() {
    this.subjectYourAnimeOne.next(this.OneAnime);
  }

  getOneYourAnime(id_user, id_anime) {
    this.http.get(this.apiYourAnime + id_user + '/' + id_anime).subscribe(
      (data) => { this.OneAnime = data[0]; this.getEmitYourAnimeOne(); },
      (error) => console.log(error)
    )
  }

  getEmitUpdateOneAnime() {
    this.subjectUpdateOneAnime.next(this.response);
  }

  getUpdateOneAnime(id_relation, id_category) {
    this.http.post(this.apiUpdateOneAnime + id_relation, JSON.stringify({ 'id_category': id_category })).subscribe(
      (data) => { this.response = data; this.getEmitUpdateOneAnime(); this.getYourAnime(); },
      (error) => console.log(error)

    )
  }

  getEmitDeleteOneAnime() {
    this.subjectDeleteOneAnime.next(this.response);
  }

  getDeleteOneAnime(id_relation) {
    this.http.delete(this.apiYourAnime + id_relation).subscribe(
      (data) => { this.response = data; this.getEmitDeleteOneAnime(); this.getYourAnime();},
      (error) => console.log(error)
    )
  }

  getEmitUpdateEpisodeAnime() {
    this.subjectUpdateEpisodeAnime.next(this.response);
  }

  getUpdateEpisodeAnime(id_relation, episodes) {
    console.log(episodes);
    this.http.get(this.apiUpdateOneAnime + id_relation + '/' + episodes).subscribe(
      (data) => { this.response = data; this.getEmitUpdateEpisodeAnime(); },
      (error) => console.log(error)
    )
  }

  getCheck(id) {
    this.getYourAnime();
    let findAnime = this.YourAnime.find((elem) =>  {
      return elem.id === id.toString();
    });

    if(findAnime != null) {
      return true;
    } else {
      return false;
    }
  }
}
