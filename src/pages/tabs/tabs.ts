import { Component } from '@angular/core';
import { IonicPage} from 'ionic-angular';
import {AnimelistPage} from "../animelist/animelist";
import {YouranimelistPage} from "../youranimelist/youranimelist";
import {ProfilPage} from "../profil/profil";

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = AnimelistPage;
  tab2Root = YouranimelistPage;
  tab3Root = ProfilPage;

  constructor() {}

}
