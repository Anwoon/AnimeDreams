import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimelistItemPage } from './animelist-item';

@NgModule({
  declarations: [
    AnimelistItemPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimelistItemPage),
  ],
})
export class AnimelistItemPageModule {}
