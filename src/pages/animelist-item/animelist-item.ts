import { Component } from '@angular/core';
import {AlertController, IonicPage, NavParams, ViewController} from 'ionic-angular';
import {AnimeProvider} from "../../providers/anime/anime";
import {AuthProvider} from "../../providers/auth/auth";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {YouranimeProvider} from "../../providers/youranime/youranime";

/**
 * Generated class for the AnimelistItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-animelist-item',
  templateUrl: 'animelist-item.html',
})
export class AnimelistItemPage {

  public idAnime;
  public check = false;
  public anime = [];

  constructor(public navParams: NavParams,
              public yourAnimeService: YouranimeProvider,
              public localNotifications: LocalNotifications,
              public viewCtrl: ViewController,
              public authService: AuthProvider,
              public alertCtrl: AlertController,
              public animeService: AnimeProvider) {
    this.idAnime =  navParams.get('id');
  }

  ionViewDidLoad() {
    this.animeService.subjectOneAnime.subscribe(
      (data) => this.anime = data,
      (error) => console.log(error),
    );
    this.animeService.getOneAnime(this.idAnime);

    this.check = this.yourAnimeService.getCheck(this.idAnime);
  }

  add(id) {
    let prompt = this.alertCtrl.create({
      title: 'Categorie',
      message: 'Sélectionner une categorie ',
      inputs : [
        {
          type:'radio',
          label:'A voir',
          value:'1'
        },
        {
          type:'radio',
          label:'En cours',
          value:'2'
        },
        {
          type:'radio',
          label:'Arreter',
          value:'3'
        },
        {
          type:'radio',
          label:'Vue',
          value:'4'
        }],
      buttons : [
        {
          text: "Cancel",
          handler: data => {
          }
        },
        {
          text: "Ajouter",
          handler: data => {
            this.animeService.subjectAddOneAnime.subscribe(
              (data) => {
                this.localNotifications.schedule({
                  id: 1,
                  title: 'Animes Dreams',
                  text: data.message,
                });
                this.viewCtrl.dismiss();
              },
              (error) => console.log(error)
            )
            this.animeService.addOneAnime(id,this.authService.idLog, data);
          }
        }]});
    prompt.present();
  }


}
