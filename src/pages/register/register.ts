import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginPage} from "../login/login";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  private register: FormGroup;
  private loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  constructor(public navCtrl: NavController,public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public auth: AuthProvider,
              private formBuilder: FormBuilder) {
    this.register = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.email]
    });
  }

  logForm(form) {
    this.loading.present();
    this.auth.getRegister(form.value).subscribe(
      (data) => {
        this.loading.dismiss();
        if(data['response'] === 'true') {
        this.navCtrl.push(LoginPage).then(data => {
          let alert = this.alertCtrl.create({
            title: 'Compte crée',
            subTitle: 'Vous pouvez désormais vous connecté',
            buttons: ['Ok']
          });
          alert.present();
        });
      } else {
          let alert = this.alertCtrl.create({
            title: 'Inscription impossible',
            subTitle: 'Username déjà utiliser',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => { this.navCtrl.setRoot(this.navCtrl.getActive().component) }
              }
            ]
          });
          alert.present();
      }},
      (error) => console.log(error),
      () => ('complete')
    )
  }

}
