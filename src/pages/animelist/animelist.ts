import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {AnimeProvider} from "../../providers/anime/anime";
import {AnimelistItemPage} from "../animelist-item/animelist-item";

/**
 * Generated class for the AnimelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-animelist',
  templateUrl: 'animelist.html',
})
export class AnimelistPage {

  number = 0;
  numberSearch = 0;
  animesList = [];
  animes = [];
  checkSearch = false;
  private loading = this.loadingCtrl.create({
    content: ''
  });
  constructor(public navCtrl: NavController,
              public auth: AuthProvider,
              public loadingCtrl: LoadingController,
              public anime: AnimeProvider,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.loading.present();
    setTimeout(() =>
    {
      this.loading.dismiss();
    }, 3000);
   this.anime.subjectAllAnime.subscribe(
      (data) => {
        this.animesList = data;
        for (let i = 0; i < 30; i++) {
          this.animes.push( this.animesList[this.number] );
          this.number++;
        }
      }
    );
    this.anime.getAllAnimes();
    this.anime.subjectSearch.subscribe(
      (data) => {
        this.checkSearch = true;
        this.animes = data;
      }
    )
  }

  ionViewCanEnter() {
    return this.auth.authenticated();
  }

  doInfinite(infiniteScroll) {
    if(this.checkSearch) {
      infiniteScroll.enable(false);
    } else {
      infiniteScroll.enable(true);
      console.log('test');
      setTimeout(() => {
        for (let i = 0; i < 30; i++) {
          this.animes.push(this.animesList[this.number]);
          this.number++;
        }
        infiniteScroll.complete();
      }, 500);
    }
  }

  itemSelected(id) {
    this.navCtrl.push(AnimelistItemPage, {
      id: id
    })
  }

  Search(event) {
    let val = event.target.value;
    if(val.length == 0) {
      this.animes = [];
      this.number = 0;
      this.checkSearch = false;
      this.anime.getAllAnimes();
    } else {
      if(val.length > 2) {
        this.anime.Search(val);
      }
    }
  }
}
