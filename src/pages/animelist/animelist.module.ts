import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimelistPage } from './animelist';

@NgModule({
  declarations: [
    AnimelistPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimelistPage),
  ],
})
export class AnimelistPageModule {}
