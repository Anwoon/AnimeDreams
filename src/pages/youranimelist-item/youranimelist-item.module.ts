import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YouranimelistItemPage } from './youranimelist-item';

@NgModule({
  declarations: [
    YouranimelistItemPage,
  ],
  imports: [
    IonicPageModule.forChild(YouranimelistItemPage),
  ],
})
export class YouranimelistItemPageModule {}
