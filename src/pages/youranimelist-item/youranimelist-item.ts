import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ViewController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {YouranimeProvider} from "../../providers/youranime/youranime";
import {LocalNotifications} from "@ionic-native/local-notifications";

/**
 * Generated class for the YouranimelistItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-youranimelist-item',
  templateUrl: 'youranimelist-item.html',
})
export class YouranimelistItemPage {

  public idAnime: number;
  public anime = [];
  private loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  constructor(public navCtrl: NavController,
              public localNotifications: LocalNotifications,
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public navParams: NavParams,
              public authService: AuthProvider,
              public yourAnimeService: YouranimeProvider) {
    this.idAnime =  navParams.get('id');
  }

  ionViewDidLoad() {
    this.yourAnimeService.subjectYourAnimeOne.subscribe(
      (data) => this.anime = data,
      (error) => console.log(error)
    )
    this.yourAnimeService.getOneYourAnime(this.authService.idLog, this.idAnime);
  }

  updateEpisode() {
    this.alertCtrl.create({
      title: 'Episodes',
      message: 'Entrer le nombre d\'épisodes vue',
      inputs : [
        {
          name: 'episodes',
          placeholder: this.anime['view_episode'],
          type: 'number'
        }],
      buttons : [
        {
          text: "Annuler",
          handler: data => {
          }
        },
        {
          text: "Ajouter",
          handler: data => {
            this.yourAnimeService.subjectUpdateEpisodeAnime.subscribe(
              (data) => {
                this.localNotifications.schedule({
                  id: 1,
                  title: 'Animes Dreams',
                  text: data.message,
                });
                this.viewCtrl.dismiss();
              },
              (error) => console.log(error)
            )
            this.yourAnimeService.getUpdateEpisodeAnime(this.anime['relation'], data.episodes);
          }
        }]}).present();
  }

  update() {
    this.alertCtrl.create({
      title: 'Categorie',
      message: 'Sélectionner une categorie ',
      inputs : [
        {
          type:'radio',
          label:'A voir',
          value:'1'
        },
        {
          type:'radio',
          label:'En cours',
          value:'2'
        },
        {
          type:'radio',
          label:'Arreter',
          value:'3'
        },
        {
          type:'radio',
          label:'Vue',
          value:'4'
        }],
      buttons : [
        {
          text: "Annuler",
          handler: data => {
          }
        },
        {
          text: "Ajouter",
          handler: data => {
            this.yourAnimeService.subjectUpdateOneAnime.subscribe(
              (data) => {
                this.localNotifications.schedule({
                  id: 1,
                  title: 'Animes Dreams',
                  text: data.message,
                });
                this.viewCtrl.dismiss();
              },
              (error) => console.log(error)
            )
            this.yourAnimeService.getUpdateOneAnime(this.anime['relation'], data);
          }
        }]}).present();
  }

  delete(id) {
    this.alertCtrl.create({
      title: 'Supprimer',
      message: 'Etes vous sur de supprimer cette anime de votre liste',
      buttons : [
        {
          text: "Annuler",
          handler: data => {
            this.loading.dismiss();
          }
        },
        {
          text: "Supprimer",
          handler: data => {
            this.yourAnimeService.subjectDeleteOneAnime.subscribe(
              (data) => {
                this.localNotifications.schedule({
                  id: 1,
                  title: 'Animes Dreams',
                  text: data.message,
                });
              },
              (error) => console.log(error)
            )
            this.yourAnimeService.getDeleteOneAnime(id);
            this.viewCtrl.dismiss();
            this.loading.dismiss();
          }
        }]}).present();
  }
}
