import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {LoginPage} from "../login/login";
import {RegisterPage} from "../register/register";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loginPage = LoginPage;
  registerPage = RegisterPage;

  constructor(public navCtrl: NavController, public storage: Storage) {}
}
