import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {HomePage} from "../home/home";

/**
 * Generated class for the ProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {

  public profil = [];
  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public localNotifications: LocalNotifications,
              public alertCtrl: AlertController,
              public navParams: NavParams,
              public authService: AuthProvider) {
  }

  ionViewDidLoad() {
    this.authService.subjectProfil.subscribe(
      (data) => this.profil = data,
          (error) => console.log(error)
    )
    this.authService.getProfil(this.authService.idLog);
  }

  EditEmail() {
    this.alertCtrl.create({
      title: 'Modifier Email',
      inputs: [
        {
          name: 'ancien_email',
          placeholder: 'Ancienne Email'
        },
        {
          name: 'new_email',
          placeholder: 'Nouvelle Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Modifier',
          handler: data => {
            if(data.ancien_email === this.profil['email']) {
              this.authService.subjectUpdateEmail.subscribe(
                (datas) => {
                  this.localNotifications.schedule({
                    id: 1,
                    title: 'Animes Dreams',
                    text: datas.message,
                  });
                  this.profil['email'] = data.new_email;
                },
                (error) => console.log(error)
              );
              this.authService.updateEmail(data.new_email);
            } else {
              this.localNotifications.schedule({
                id: 1,
                title: 'Animes Dreams',
                text: 'Ancienne Email non valide',
              });
            }
          }
        }
      ]
    }).present();
  }

  EditUsername() {
    this.alertCtrl.create({
      title: 'Modifier Username',
      inputs: [
        {
          name: 'ancien_username',
          placeholder: 'Ancien Pseudo'
        },
        {
          name: 'new_username',
          placeholder: 'Nouveau Pseudo'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Modifier',
          handler: data => {
            if(data.ancien_username === this.profil['username']) {
              this.authService.subjectUpdateUsername.subscribe(
                (datas) => {
                  this.localNotifications.schedule({
                    id: 1,
                    title: 'Animes Dreams',
                    text: datas.message,
                  });
                  this.profil['username'] = data.new_username;
                },
                (error) => console.log(error)
              );
              this.authService.updateUsername(data.new_username);
            } else {
              this.localNotifications.schedule({
                id: 1,
                title: 'Animes Dreams',
                text: 'Ancien Pseudo non valide',
              });
            }
          }
        }
      ]
    }).present();
  }

  EditPassword() {
    let password = this.alertCtrl.create({
      title: 'Modifier Mot de passe',
      inputs: [
        {
          name: 'new_password',
          type:'password',
          placeholder: 'Nouveau Mot de passe'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Modifier',
          handler: data => {
              this.authService.subjectUpdatePassword.subscribe(
                (datas) => {
                  this.localNotifications.schedule({
                    id: 1,
                    title: 'Animes Dreams',
                    text: datas.message,
                  });
                },
                (error) => console.log(error)
              );
              this.authService.updatePassword(data.new_password);
          }
        }
      ]
    });
    password.present();
  }

  Disconnect () {
    let loading = this.loadingCtrl.create({
    content: 'Déconnexion'
  });

    localStorage.removeItem('username');
    localStorage.removeItem('user_id');
    this.authService.idLog = null;
    this.authService.usernameLog = null;
    this.authService.logout();
    loading.present();

    setTimeout(() => {
      loading.dismiss();
      location.reload();
    }, 3000);
  }
}
