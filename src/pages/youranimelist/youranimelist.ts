import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {YouranimeProvider} from "../../providers/youranime/youranime";
import {YouranimelistItemPage} from "../youranimelist-item/youranimelist-item";

/**
 * Generated class for the YouranimelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-youranimelist',
  templateUrl: 'youranimelist.html',
})
export class YouranimelistPage {

  public number: number;
  private animesList = [];
  private animes: any = [];
  constructor(public navCtrl: NavController,
              public yourAnimeService: YouranimeProvider,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.yourAnimeService.subjectYourAnime.subscribe(
      (data) => this.animes = data,
      (error) => console.log(error)
    )
  this.yourAnimeService.getYourAnime();
  }

  itemSelected(id) {
    this.navCtrl.push(YouranimelistItemPage, {
      id: id
    })
  }

}
