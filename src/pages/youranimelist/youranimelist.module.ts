import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YouranimelistPage } from './youranimelist';

@NgModule({
  declarations: [
    YouranimelistPage,
  ],
  imports: [
    IonicPageModule.forChild(YouranimelistPage),
  ],
})
export class YouranimelistPageModule {}
