import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import {TabsPage} from "../tabs/tabs";
import {Storage} from "@ionic/storage";
import {LocalNotifications} from "@ionic-native/local-notifications";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private login : FormGroup;
  private loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  constructor(private navCtrl: NavController,
              private storage: Storage,
              private localNotifications: LocalNotifications,
              private loadingCtrl: LoadingController,
              private alertCtrl:AlertController,
              private auth: AuthProvider,
              private formBuilder: FormBuilder) {
    this.login = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  logForm(form) {
    this.loading.present();
      this.auth.getLogin(form.value).subscribe(
        (data) => {
          this.loading.dismiss();
          if (data['response'] === true) {
            this.auth.login();
            this.auth.usernameLog = data['user_name'];
            this.auth.idLog = data['user_id'];
            localStorage.setItem('username', data['user_name']);
            localStorage.setItem('user_id', data['user_id']);
            this.navCtrl.push(TabsPage);
          } else {
            let alert = this.alertCtrl.create({
              title: 'Connection impossible',
              subTitle: data['message'],
              buttons: [
                {
                  text: 'Ok',
                  role: 'cancel',
                  handler: () => {
                    this.navCtrl.setRoot(this.navCtrl.getActive().component)
                  }
                }
              ]
            });
            alert.present();
          }
        },
        (error) => console.log(error),
        () => ('complete')
      )
    }

  RecoveryPassword() {
    this.alertCtrl.create({
      title: 'Recuperation de votre mot de passe',
      message: 'veuillez renseigner l\'email associer a votre compte Anime Dreams',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
        },
        {
          text: 'Envoyer',
          handler: data => {
              this.auth.subjectRecoveryPassword.subscribe(
                (datas) => {
                  this.localNotifications.schedule({
                    id: 1,
                    title: 'Animes Dreams',
                    text: datas.message,
                  });
                },
                (error) => console.log(error)
              );
              this.auth.recoveryPassword(data.email);
          }
        }
      ]
    }).present();
  }
}
