import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {HomePage} from "../pages/home/home";
import {TabsPage} from "../pages/tabs/tabs";
import {AnimelistPage} from "../pages/animelist/animelist";
import {AuthProvider} from "../providers/auth/auth";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public authService : AuthProvider) {
    if (localStorage.getItem('username') != null && localStorage.getItem('user_id') != null) {
      this.authService.login();
      this.authService.idLog = localStorage.getItem('user_id');
      this.authService.usernameLog = localStorage.getItem('username');
      this.rootPage = TabsPage;
    }
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
