import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthProvider } from '../providers/auth/auth';
import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import { AnimeProvider } from '../providers/anime/anime';
import {YouranimelistPage} from "../pages/youranimelist/youranimelist";
import {AnimelistPage} from "../pages/animelist/animelist";
import {ProfilPage} from "../pages/profil/profil";
import {YouranimeProvider} from "../providers/youranime/youranime";
import {AnimelistItemPage} from "../pages/animelist-item/animelist-item";
import {TabsPage} from "../pages/tabs/tabs";
import {YouranimelistItemPage} from "../pages/youranimelist-item/youranimelist-item";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {IonicStorageModule} from "@ionic/storage";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    YouranimelistPage,
    AnimelistPage,
    ProfilPage,
    AnimelistItemPage,
    TabsPage,
    YouranimelistItemPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    YouranimelistPage,
    AnimelistPage,
    ProfilPage,
    AnimelistItemPage,
    TabsPage,
    YouranimelistItemPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    AnimeProvider,
    YouranimeProvider,
    LocalNotifications
  ]
})
export class AppModule {}
